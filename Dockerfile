#FROM ubuntu:16.04 as builder
FROM registry.gitlab.com/ilja7777/ubuntu-web:1.0 as builder
MAINTAINER Mark Slepkov self@mark-slepkov.ru
#EXPOSE 8000
#RUN apt-get update -qy && apt-get install -y python-dev python-pip python-virtualenv python3-dev python3-pip git ssh libjpeg8-dev
#RUN apt-get update -qy && apt-get install -y libpq-dev
ENV PYTHONUNBUFFERED 1
ARG SSH_KEY
ENV SSH_KEY=$SSH_KEY
RUN mkdir /project && mkdir ~/.ssh
RUN printf "Host * \n   StrictHostKeyChecking no" > ~/.ssh/config
WORKDIR /project
COPY celtest ./celtest
COPY setup.py ./setup.py
RUN echo "$SSH_KEY" > ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa
#RUN eval $(ssh-agent) > /dev/null && ssh-add ~/.ssh/id_rsa
RUN virtualenv --python=/usr/bin/python3 ./venv && \
    ./venv/bin/pip install --upgrade pip && \
    ./venv/bin/pip install . --process-dependency-links --upgrade && \
    ./venv/bin/pip install gunicorn


FROM registry.gitlab.com/ilja7777/ubuntu-web:1.0 as runtime
#FROM ubuntu:16.04 as runtime
ENV PYTHONUNBUFFERED 1
COPY --from=builder /project /project
WORKDIR /project/celtest
#EXPOSE 8000
#RUN apt-get update -qy && apt-get install -y python-dev python-pip python-virtualenv python3-dev python3-pip libjpeg8-dev#
#RUN apt-get update -qy && apt-get install -y libpq-dev
CMD ../venv/bin/python ./manage.py migrate --noinput && \
#    ../venv/bin/python ./manage.py collectstatic --noinput && \
#    ../venv/bin/python ./manage.py initadmin && \
    ../venv/bin/python ./manage.py runserver 0.0.0.0:8000
#    ../venv/bin/gunicorn -w 2 celtest.wsgi -b 0.0.0.0:8000
