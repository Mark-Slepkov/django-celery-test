from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from celery.schedules import crontab

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'celtest.settings')

app = Celery('celtest')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(2.0, make_sitemap_xml.s(), name='add every 2 sec')
    sender.add_periodic_task(3.0, make_market_yml.s(), expires=10)


@app.task(bind=True)
def make_sitemap_xml(self):
    print('Sitemap.xml')


@app.task(bind=True)
def make_market_yml(self):
    print('Market.yml')