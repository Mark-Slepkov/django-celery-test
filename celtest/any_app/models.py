from django.db import models
from celtest import celery_app as app
# from .tasks import anything_is_saved


class Anything(models.Model):
    title = models.CharField(max_length=300)
    description = models.TextField()
    checksum = models.CharField(max_length=300, default=None, null=True, blank=True)

    def save(self, *args, **kwargs):
        result = super().save(*args, **kwargs)
        app.send_task('anything_is_saved', args=(self.id,))
        return result
