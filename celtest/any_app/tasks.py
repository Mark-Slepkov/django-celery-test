from __future__ import absolute_import, unicode_literals
from celtest import celery_app as app
from .models import Anything

@app.task(name='anything_is_saved')
def anything_is_saved(pk):
    anything = Anything.objects.get(pk=pk)
    print('Anything is saved Title: {0}, Description: {1}'.format(anything.title, anything.description))
    Anything.objects.filter(pk=pk).update(checksum='123123123123')
    # anything.save()


