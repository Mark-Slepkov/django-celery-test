from django.contrib import admin
from .models import Anything


@admin.register(Anything)
class AnythingAdmin(admin.ModelAdmin):
    fields = ('title', 'description', 'checksum')
    readonly_fields = ('checksum',)

