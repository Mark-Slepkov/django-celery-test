from django.apps import AppConfig


class AnyAppConfig(AppConfig):
    name = 'any_app'
